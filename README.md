![Header](https://i.imgur.com/9VG3mI5.png)

[![Documentation](https://i.imgur.com/7QDbrIS.png)](https://andrei1058.gitbook.io/bedwarsproxy/) [![Report a Bug](https://i.imgur.com/Z1qOYLC.png)](https://gitlab.com/andrei1058/bedwarsproxy/-/issues) [![API](https://i.imgur.com/JfMTMMc.png)](http://javadoc.andrei1058.com/BedWarsProxy/) [![Discord](https://i.imgur.com/yBySzkU.png)](https://discord.gg/XdJfN2X)

**BedWarsProxy** is a plugin for Bungeecord networks that are running BedWars1058 in BUNGEE mode. This plugin provides features for lobby servers: join gui/ signs, placeholders and more.

**IT'S A FORK** This plugin is a fork for the need of CraftTok, this plugin exist because we need two instances of the proxy plugin.

![Signs](https://i.imgur.com/ggNRp4D.png?1)

**FEATURES**
- dynamic game signs
- static game signs
- global arena selector
- per group arena selector
- arena rejoin system
- admin /bw tp <player> command to catch cheaters
- per player language system in sync with arenas
- PAPI placeholders
- internal party system
- API for developers

**HOW TO USE**

All the information you need can be found on its [documentation/ wiki](https://andrei1058.gitbook.io/bedwarsproxy/).
